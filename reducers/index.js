import { actionTypes } from "../actions";

export const initialData = {};

const reducer = (state = initialData, { type, data }) => {
    switch(type) {
        default:
            return state;
    }
}

export default reducer;